clear all
close all
clc

%{
% Analogiczne rozwiązania dla nadwozia, kół, nawierzchni.
%    _____________
%   |             |
%   |z_x2    z_x1 |
%   |    XXXX     |
%   |   XXXXXX    |
%   |    XXXX     |
%   |z_x4    z_x3 |
%   |_____________|
% 
% Wyznaczone kąty: 
% - roll_front, 
% - roll_rear,
% - pitch_left,
% - pitch_right.
% 
%}

%% IMPORT DANYCH
randomFullModelData = readtable('src/vehicle_visualisation/data/randomFullModelData.csv');
z_x1 = randomFullModelData.z_s1; % PP
z_x2 = randomFullModelData.z_s2; % LP
z_x3 = randomFullModelData.z_s3; % PT
z_x4 = randomFullModelData.z_s4; % LT

%% PRZEKSZTAŁCENIA 
roll_front = (z_x2 - z_x1) * 90; % ROLL FRONT AXIS
roll_rear = (z_x4 - z_x3) * 90;  % ROLL REAR AXIS

pitch_left = (z_x2 - z_x4) * 90;  % PITCH LEFT SIDE
pitch_right = (z_x1 - z_x3) * 90; % PITCH RIGHT SIDE

%% WYKRESY
figure(1);
plot(roll_front);
hold on
plot(roll_rear);
legend('front', 'rear');
title('roll');

figure(2);
plot(pitch_left);
hold on
plot(pitch_right);
legend('left', 'right');
title('pitch');