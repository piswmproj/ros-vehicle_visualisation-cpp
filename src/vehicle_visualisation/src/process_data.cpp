#include "ros/ros.h"
#include "vehicle_visualisation/Raw.h"
#include "vehicle_visualisation/Position.h"
#include "settings.h"

vehicle_visualisation::Raw data;
static bool new_data = false;

void raw_data_sub_callback(const vehicle_visualisation::Raw::ConstPtr& msg)
{
  memcpy(&data, &(*msg), sizeof(*msg));
  new_data = true;
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "process_data");
  ros::NodeHandle n;
  ros::Subscriber raw_data_sub = n.subscribe("raw_data", SUB_BUFFER, raw_data_sub_callback);
  ros::Publisher position_data_pub = n.advertise<vehicle_visualisation::Position>("position_data", PUB_BUFFER);
  ros::Rate loop_rate(LOOP_RATE_HZ);

  vehicle_visualisation::Position msg;

  while (ros::ok())
  {
    if (new_data)
    {
      // process data
      msg.roll = (data.z_s2 - data.z_s1) * 90;
      msg.pitch = (data.z_s2 - data.z_s4) * 90;

      // rewrite data
      msg.pos_x = data.pos_x;
      msg.pos_y = data.pos_y;
      msg.yaw = data.yaw;

      position_data_pub.publish(msg);
      new_data = false;

#ifdef DEBUG_INFO
      ROS_INFO("\nI heard: \n[%f] [%f] [%f] [%f]\n[%f] [%f] [%f] [%f]\n[%f] [%f] [%f] [%f]\n[%f] [%f] [%f]\n\n", \
      data.z_r1, data.z_r1, data.z_r1, data.z_r1,\
      data.z_u1, data.z_u1, data.z_u1, data.z_u1,\
      data.z_s1, data.z_s1, data.z_s1, data.z_s1,\
      data.pos_x, data.pos_y, data.yaw);

      ROS_INFO("SENT: %f %f %f %f %f", \
        msg.roll, msg.pitch, msg.pos_x, msg.pos_y, msg.yaw);
#endif
    }
  
    ros::spinOnce();
    loop_rate.sleep();
  }

  return 0;
}
