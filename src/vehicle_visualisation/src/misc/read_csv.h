#ifndef _READ_CSV_H
#define _READ_CSV_H

#include <string>
#include <fstream>
#include <vector>
#include <utility> // std::pair
#include <stdexcept> // std::runtime_error
#include <sstream> // std::stringstream

typedef std::vector<std::pair<std::string, std::vector<float>>> csv_data;

std::vector<std::pair<std::string, std::vector<float>>> read_csv(std::string filename);

#endif  //_READ_CSV_H