#include "ros/ros.h"
#include "vehicle_visualisation/Raw.h"
#include "misc/read_csv.h"
#include <ros/package.h>
#include "settings.h"

#define SAMPLE_SIZE    5000   // Number of samples

#define CSV_DATA_PATH "/data/randomFullModelData.csv"

/*
 * @brief
 */
void get_data(vehicle_visualisation::Raw &msg, csv_data csv_data, int i)
{
  msg.z_r1 = csv_data[0].second[i];
  msg.z_r2 = csv_data[1].second[i];
  msg.z_r3 = csv_data[2].second[i];
  msg.z_r4 = csv_data[3].second[i];
  msg.z_u1 = csv_data[4].second[i];
  msg.z_u2 = csv_data[5].second[i];
  msg.z_u3 = csv_data[6].second[i];
  msg.z_u4 = csv_data[7].second[i];
  msg.z_s1 = csv_data[8].second[i];
  msg.z_s2 = csv_data[9].second[i];
  msg.z_s3 = csv_data[10].second[i];
  msg.z_s4 = csv_data[11].second[i];
  msg.pos_x = csv_data[12].second[i];
  msg.pos_y = csv_data[13].second[i];
  msg.yaw = csv_data[14].second[i];
}


int main(int argc, char **argv)
{
  vehicle_visualisation::Raw msg;
  csv_data csv_data;
  int count = 0;
  bool stop = false;

  const std::string csv_data_path = ros::package::getPath(PACKAGE_NAME) + CSV_DATA_PATH;
  csv_data = read_csv(csv_data_path);

  ros::init(argc, argv, "data_acquisition");
  ros::NodeHandle n;
  ros::Publisher raw_data_pub = n.advertise<vehicle_visualisation::Raw>("raw_data", PUB_BUFFER);
  ros::Rate loop_rate(LOOP_RATE_HZ);
  
  while (ros::ok())
  {
    if (count >= SAMPLE_SIZE) break;
    get_data(msg, csv_data, count++);
    raw_data_pub.publish(msg);

#ifdef DEBUG_INFO
    ROS_INFO("\nSENT: \n%f %f %f %f\n%f %f %f %f\n%f %f %f %f\n%f %f %f\n\n", \
      msg.z_r1, msg.z_r1, msg.z_r1, msg.z_r1,\
      msg.z_u1, msg.z_u1, msg.z_u1, msg.z_u1,\
      msg.z_s1, msg.z_s1, msg.z_s1, msg.z_s1,\
      msg.pos_x, msg.pos_y, msg.yaw);
#endif

    ros::spinOnce();
    loop_rate.sleep();
  }

  return 0;
}