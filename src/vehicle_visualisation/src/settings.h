#ifndef _SETTINGS_H
#define _SETTINGS_H

#define PACKAGE_NAME            "vehicle_visualisation"

#define LOOP_RATE_HZ            500    // We send messages with 500 Hz frequency
#define PUB_BUFFER              1000   // publication bufer up to N unsent messages
#define SUB_BUFFER              1000   // subscription bufer up to N unsent messages

#define CAR_IMAGE_PATH          "./car.png"  // relative path to image of a car  
                                       // from the src/visualisation directory
#define FRONT_IMAGE_PATH        "./car_front.jpg"
#define CAR_SIDE_IMAGE_PATH     "./car_side.jpg"                                       

#define DEBUG_INFO

#endif // _SETTINGS_H