#include "ros/package.h"
#include "rotate.h"
#include "../settings.h"

using namespace cv;

static Mat image(600,1150, CV_8UC3, Scalar(255,255,255)); //full window
static Mat car_front,car_side;
static Mat frameRotated2, frameRotated3;
static Mat frame, frameRotated,frame_side,frameRotated_side;
static Point pt = Point(1,1); //main point
static Point pt2 = Point(550,1); //main point
static int x_pos,y_pos;

/*
 * @brief initialize simulation, function must be called
 */
int sim2_init() {
	int ret = 0;
  struct stat buffer;   

	const std::string front_image_path = ros::package::getPath(PACKAGE_NAME) + "/src/visualisation/" + FRONT_IMAGE_PATH;
  const std::string car_side_image_path = ros::package::getPath(PACKAGE_NAME) + "/src/visualisation/" + CAR_SIDE_IMAGE_PATH;

  if (stat (front_image_path.c_str(), &buffer) == 0)
		car_front = imread(front_image_path, 1);
	else
	{
		std::cout << "Could not open file " << front_image_path << std::endl;
		ret = -1;
	}

  if (stat (car_side_image_path.c_str(), &buffer) == 0)
		car_side = imread(car_side_image_path, 1);
	else
	{
		std::cout << "Could not open file " << car_side_image_path << std::endl;
		ret = -1;
	}

	return ret;
}

Mat rotate_front(Mat src, float angle)
{
int diagonal = (int)sqrt(src.cols*src.cols+src.rows*src.rows);
int newWidth = diagonal;
int newHeight =diagonal;

int offsetX = (newWidth - src.cols) / 2;
int offsetY = (newHeight - src.rows) / 2;
Mat targetMat(newWidth, newHeight, src.type());
Point2f src_center(targetMat.cols/2.0F, targetMat.rows/2.0F);

src.copyTo(frame);
double radians = angle * M_PI / 180.0;
double sin = abs(std::sin(radians));
double cos = abs(std::cos(radians));

frame.copyTo(targetMat.rowRange(offsetY, offsetY + frame.rows).colRange(offsetX, offsetX + frame.cols));
Mat rot_mat = getRotationMatrix2D(src_center, angle, 1.0);
warpAffine(targetMat, frameRotated, rot_mat, targetMat.size());
 //Calculate bounding rect and for exact image
 //Reference:- https://stackoverflow.com/questions/19830477/find-the-bounding-rectangle-of-rotated-rectangle/19830964?noredirect=1#19830964
Rect bound_Rect(frame.cols,frame.rows,0,0);

    int x1 = offsetX;
    int x2 = offsetX+frame.cols;
    int x3 = offsetX;
    int x4 = offsetX+frame.cols;

    int y1 = offsetY;
    int y2 = offsetY;
    int y3 = offsetY+frame.rows;
    int y4 = offsetY+frame.rows;

    Mat co_Ordinate = (Mat_<double>(3,4) << x1, x2, x3, x4,
                                            y1, y2, y3, y4,
                                            1,  1,  1,  1 );
    Mat RotCo_Ordinate = rot_mat * co_Ordinate;

    for(int i=0;i<4;i++){
       if(RotCo_Ordinate.at<double>(0,i)<bound_Rect.x)
         bound_Rect.x=(int)RotCo_Ordinate.at<double>(0,i); //access smallest 
       if(RotCo_Ordinate.at<double>(1,i)<bound_Rect.y)
        bound_Rect.y=RotCo_Ordinate.at<double>(1,i); //access smallest y
     }

     for(int i=0;i<4;i++){
       if(RotCo_Ordinate.at<double>(0,i)>bound_Rect.width)
         bound_Rect.width=(int)RotCo_Ordinate.at<double>(0,i); //access largest x
       if(RotCo_Ordinate.at<double>(1,i)>bound_Rect.height)
        bound_Rect.height=RotCo_Ordinate.at<double>(1,i); //access largest y
     }

    bound_Rect.width=bound_Rect.width-bound_Rect.x;
    bound_Rect.height=bound_Rect.height-bound_Rect.y;

    Mat cropedResult;
    Mat ROI = frameRotated(bound_Rect);
    ROI.copyTo(cropedResult);

   

    //imshow("Result", cropedResult);
    //imshow("frame", frame);
    //imshow("rotated frame", frameRotated);
    //waitKey(200);
     return frameRotated;
}

Mat rotate_side(Mat src, float angle)
{
   
int diagonal = (int)sqrt(src.cols*src.cols+src.rows*src.rows);
int newWidth = diagonal;
int newHeight =diagonal;

int offsetX = (newWidth - src.cols) / 2;
int offsetY = (newHeight - src.rows) / 2;
Mat targetMat(newWidth, newHeight, src.type());
Point2f src_center(targetMat.cols/2.0F, targetMat.rows/2.0F);

src.copyTo(frame_side);
double radians = angle * M_PI / 180.0;
double sin = abs(std::sin(radians));
double cos = abs(std::cos(radians));

frame_side.copyTo(targetMat.rowRange(offsetY, offsetY + frame_side.rows).colRange(offsetX, offsetX + frame_side.cols));
Mat rot_mat = getRotationMatrix2D(src_center, angle, 1.0);
warpAffine(targetMat, frameRotated_side, rot_mat, targetMat.size());
 //Calculate bounding rect and for exact image
 //Reference:- https://stackoverflow.com/questions/19830477/find-the-bounding-rectangle-of-rotated-rectangle/19830964?noredirect=1#19830964
Rect bound_Rect(frame_side.cols,frame_side.rows,0,0);

    int x1 = offsetX;
    int x2 = offsetX+frame_side.cols;
    int x3 = offsetX;
    int x4 = offsetX+frame_side.cols;

    int y1 = offsetY;
    int y2 = offsetY;
    int y3 = offsetY+frame_side.rows;
    int y4 = offsetY+frame_side.rows;

    Mat co_Ordinate = (Mat_<double>(3,4) << x1, x2, x3, x4,
                                            y1, y2, y3, y4,
                                            1,  1,  1,  1 );
    Mat RotCo_Ordinate = rot_mat * co_Ordinate;

    for(int i=0;i<4;i++){
       if(RotCo_Ordinate.at<double>(0,i)<bound_Rect.x)
         bound_Rect.x=(int)RotCo_Ordinate.at<double>(0,i); //access smallest 
       if(RotCo_Ordinate.at<double>(1,i)<bound_Rect.y)
        bound_Rect.y=RotCo_Ordinate.at<double>(1,i); //access smallest y
     }

     for(int i=0;i<4;i++){
       if(RotCo_Ordinate.at<double>(0,i)>bound_Rect.width)
         bound_Rect.width=(int)RotCo_Ordinate.at<double>(0,i); //access largest x
       if(RotCo_Ordinate.at<double>(1,i)>bound_Rect.height)
        bound_Rect.height=RotCo_Ordinate.at<double>(1,i); //access largest y
     }

    bound_Rect.width=bound_Rect.width-bound_Rect.x;
    bound_Rect.height=bound_Rect.height-bound_Rect.y;

    Mat cropedResult;
    Mat ROI = frameRotated_side(bound_Rect);
    ROI.copyTo(cropedResult);
    //imshow("Result", cropedResult);
    //imshow("frame", frame);
    //imshow("rotated frame pitch", frameRotated);
    //waitKey(200);
    return frameRotated_side;
}


void show_rotates(float Roll_angle, float Pitching_angle){    
    frameRotated2 = rotate_front(car_front, Roll_angle);
    
    //car.copyTo(image(Rect(pt.x-x_pos,pt.y-y_pos,car.cols,car.rows)));
    frameRotated3 = rotate_side(car_side,Pitching_angle);

    frameRotated2.copyTo(image(Rect(pt.x,pt.y,frameRotated2.cols,frameRotated2.rows)));
    frameRotated3.copyTo(image(Rect(pt2.x,pt2.y,frameRotated3.cols,frameRotated3.rows)));
    //imshow("Rolling", frameRotated2);
    //imshow("Pitching", frameRotated3);
    imshow("result", image);
    waitKey(1);
}
