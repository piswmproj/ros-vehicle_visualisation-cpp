#include "ros/package.h"
#include "position.h"
#include "../settings.h"

using namespace cv;

static Point pt = Point(200,500); //main point
static Mat image(1000,1000, CV_8UC3, Scalar(255,255,255)); //full window
static Mat dst(1000,1000, CV_8UC3, Scalar(255,255,255)); //temp window to perfor functions
static Point circle_pt = Point(200,400); //point for clearing position (punkt narożnika wyświetlającego obraz auta)
static Point pt2 = Point(200,420); //init point for clearing rectangle (przeciwległy narożnik)
static Point trajectory_pt = Point(200,400);

static Mat car;
static Mat trajectory;
static Mat car_ref; 

static int thickness =-1,lineType=8, shift=0;
static float distance;
static int x_trans,y_trans;


/*
 * @brief initialize simulation, function must be called
 */
int sim_init() {
	int ret = 0;
	const std::string car_image_path = ros::package::getPath(PACKAGE_NAME) + "/src/visualisation/" + CAR_IMAGE_PATH;
	struct stat buffer;   
	if (stat (car_image_path.c_str(), &buffer) == 0)
		car = imread(car_image_path,1);
	else
	{
		std::cout << "Could not open file " << car_image_path << std::endl;
		ret = -1;
	}
		
	car_ref = car;
	trajectory_pt.x = 200;
	trajectory_pt.y = 400+car.rows/2;
	distance = car.rows/2;

	return ret;
}


/*
 * @brief rotate object of the car based on rot angle of the car
 */
static Mat rotate(Mat src, double angle, int *x_pos, int *y_pos) {
	Mat dst;
	Point2f pt(src.cols/2., src.rows/2.);  
	//Point2f pt(0,0);  
	angle = -angle; // invert data from csv file
	Mat r = getRotationMatrix2D(pt, angle, 1.0);
	Rect bbox = RotatedRect(Point2f(), src.size(), angle).boundingRect2f();
	// adjust transformation matrix
	r.at<double>(0,2) += bbox.width/2.0 - src.cols/2.0;
	r.at<double>(1,2) += bbox.height/2.0 - src.rows/2.0;
	warpAffine(src, dst, r, bbox.size(),INTER_LINEAR, BORDER_CONSTANT, Scalar(255,255,255));
	*x_pos = r.at<double>(0,2);
	*y_pos = r.at<double>(1,2);
	return dst;
}


/*
 * @brief function translating window when car is close to edge
 */
Mat translate_window(Mat image, Point &pt, Mat car, float &pos_x, float &pos_y) {
	Mat dst;
	static int x_right,y_top,x_lef,y_down;
	Mat par= Mat(2, 3, CV_64FC1);
	dst = image;
	if (pt.y<120) {
		par.at<double>(0,0)=  1;  //p1
		par.at<double>(1,0)=  0;  //p2;
		par.at<double>(0,1)=  0; //p3;
		par.at<double>(1,1)=  1; //p4;
		par.at<double>(0,2)=  0;  //p5;
		par.at<double>(1,2)=  10;   //p6;
		warpAffine(image,dst,par, image.size(),INTER_LINEAR, BORDER_CONSTANT, Scalar(255,255,255));
		image = dst;
		pt.y = pt.y+10;
		pos_y = pos_y+10;          
	} else if (pt.y>(1000-car.rows-20)) {
		par.at<double>(0,0)=  1;  //p1
		par.at<double>(1,0)=  0;  //p2;
		par.at<double>(0,1)=  0; //p3;
		par.at<double>(1,1)=  1; //p4;
		par.at<double>(0,2)=  0;  //p5;
		par.at<double>(1,2)=  -10;   //p6;
		warpAffine(image,dst,par, image.size(),INTER_LINEAR, BORDER_CONSTANT, Scalar(255,255,255));
		image = dst;
		pt.y = pt.y-10;
		pos_y = pos_y-10;        
	} else if (pt.x<50) {
		par.at<double>(0,0)=  1;  //p1
		par.at<double>(1,0)=  0;  //p2;
		par.at<double>(0,1)=  0; //p3;
		par.at<double>(1,1)=  1; //p4;
		par.at<double>(0,2)=  10;  //p5;
		par.at<double>(1,2)=  0;   //p6;
		warpAffine(image,dst,par, image.size(),INTER_LINEAR, BORDER_CONSTANT, Scalar(255,255,255));
		image = dst;
		pt.x = pt.x+10; 
		pos_x = pos_x+10;
	} else if (pt.x>(1000-car.cols-50)) {
		par.at<double>(0,0)=  1;  //p1
		par.at<double>(1,0)=  0;  //p2;
		par.at<double>(0,1)=  0; //p3;
		par.at<double>(1,1)=  1; //p4;
		par.at<double>(0,2)=  -10;  //p5;
		par.at<double>(1,2)=  0;   //p6;
		warpAffine(image,dst,par, image.size(),INTER_LINEAR, BORDER_CONSTANT, Scalar(255,255,255));
		image = dst;
		pt.x = pt.x-10;
		pos_x = pos_x-10;
	}
	return image;
}


/*
 * @brief
 */
int show_position(float read_x, float read_y, float read_roll) {
    float diff;
    double temp_orient;
    static float prev_x,prev_y;
    static float pos_x,pos_y;
    static int count;
    count++;
    //calculate cleating posiotions
    circle_pt.x = pt.x-x_trans;
    circle_pt.y = pt.y-y_trans;
    pt2.x = circle_pt.x+car.cols;
    pt2.y = circle_pt.y+car.rows;
    rectangle(image, circle_pt, pt2, Scalar(255,255,255),thickness,lineType,shift); //clear previous position
    //rectangle(image, pt, pt2, Scalar(255,255,255),thickness,lineType,shift); //clear previous position      
    if (count==1) { //init positions
        pt.x = 500 + read_x*10;
        pt.y = 400 + read_y*10;
        pos_x = pt.x;
        pos_y = pt.y;
    } else {
        diff = read_x*10 - prev_x;
        pos_x += diff;
        pt.x = pos_x;
        diff = read_y*10 - prev_y;

        pos_y += diff;
        pt.y = pos_y;
        prev_x = read_x*10;
        prev_y = read_y*10;  
    }
    
    image = translate_window(image, pt, car, pos_x,pos_y);
    pt.x = pos_x;
    pt.y = pos_y;
    //************calculate rotation************
    temp_orient = read_roll* M_PI / 180; //radians
    trajectory_pt.x = distance*sin(temp_orient) + pt.x;
    trajectory_pt.y = distance*cos(temp_orient) + pt.y;
    circle(image, trajectory_pt, 32, Scalar(0,102,204), FILLED, LINE_8); // print trajectory

    car = rotate(car_ref,read_roll, &x_trans, &y_trans); //rotate car
    car.copyTo(image(Rect(pt.x-x_trans,pt.y-y_trans,car.cols,car.rows)));

    imshow("Start project with opencv", image);
    int c = waitKey(1);
    //if ((c == 32)||(c == 27)) break;

    return -1;
}

