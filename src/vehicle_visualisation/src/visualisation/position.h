#ifndef _POSITION_H
#define _POSITION_H

#include<opencv2/opencv.hpp>
#include<iostream>
#include<stdio.h>
#include<unistd.h>
#include<math.h>
#include<string>
#include <sys/stat.h>

int show_position(float pos_x, float pos_y, float roll);
int sim_init();
#endif