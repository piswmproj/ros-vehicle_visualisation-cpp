#include "ros/ros.h"
#include "vehicle_visualisation/Position.h"
#include "settings.h"
#include "visualisation/position.h"
#include "visualisation/rotate.h"

vehicle_visualisation::Position data;
static bool new_data = false;


void position_data_sub_callback(const vehicle_visualisation::Position::ConstPtr& msg)
{
  memcpy(&data, &(*msg), sizeof(*msg));
  new_data = true;
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "visualisation");
  ros::NodeHandle n;
  ros::Subscriber position_data_sub = n.subscribe("position_data", SUB_BUFFER, position_data_sub_callback);
  ros::Rate loop_rate(LOOP_RATE_HZ);

  int err = sim_init();
  if (err)
  {
      std::cout << "Error in function sim_init(), " << err << std::endl;
			return -1;
  }

  err = sim2_init();
  if (err)
  {
      std::cout << "Error in function sim2_init(), " << err << std::endl;
			return -1;
  }

  while (ros::ok())
  {
    if (new_data)
    {
      show_position(data.pos_x, data.pos_y, data.yaw);
      show_rotates(data.roll, data.pitch);
      new_data = false;

#ifdef DEBUG_INFO
    ROS_INFO("SENT: [%f] [%f] [%f] [%f] [%f]", \
      data.roll, data.pitch, data.pos_x, data.pos_y, data.yaw);
#endif
    }

    ros::spinOnce();
    loop_rate.sleep();
  }

  return 0;
}
