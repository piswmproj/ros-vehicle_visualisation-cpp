# Overview
This repository contains solution visualising the localisation of vehicle and position of the vehicle suspension based on the acceleration measurements. At this step of project developement, acceleration data are being obtained from the text file. Project is based on the three nodes: 
* data_acquisition
* process_data
* visualisation

##### data_acquisition
This node is responsible for reading data from the text file and sending raw data to the 'process_data' node by publishing it on topic 'raw_data'.

##### process_data
In this node the data from accelerometers are being processed to get vehicle position (with reference to primary position) and suspension travel. Processed data are being published on topic 'position_data'.

##### visualisation
The data from 'process_data' node are being visualised there using OpenCV library.

# Compile and run
Install ROS enviroment following instructions included under this link:
http://wiki.ros.org/ROS/Tutorials/InstallingandConfiguringROSEnvironment

Clone this repository to your home directory

    cd ~
    git clone https://gitlab.com/piswmproj/ros-vehicle_visualisation-cpp
    cd ros-vehicle_visualisation-cpp

Compile project
    
    catkin_make
    catkin_make install
   
Open four terminals (using Terminator is recommended), configure each with following command:

    source ~/ros-vehicle_visualisation-cpp/devel/setup.bash
    
> You can also add this line to your ~/.bashrc file to avoid configuring each terminal separately. If you want to edit your .bashrc file, execute following command:

> echo $'\n\nsource ~/ros-vehicle_visualisation-cpp/devel/setup.bash' >> ~/.bashrc

Run following commands in separate terminals in following order:

    roscore
    rosrun vehicle_visualisation process_data
    rosrun vehicle_visualisation visualisation
    rosrun vehicle_visualisation data_acquisition
    
# Troubleshooting
In case of complications with building and running this project, check the correctness of ROS enviroment installation.